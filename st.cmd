#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_digitelqpce
#
require vac_ctrl_digitelqpce,1.5.0


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_digitelqpce_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: MEBT-010:Vac-VEPI-10001
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_ctrl_digitelqpce_moxa.iocsh", "DEVICENAME = MEBT-010:Vac-VEPI-10001, IPADDR = moxa-vac-mebt.tn.esss.lu.se, PORT = 4005")

#
# Device: MEBT-010:Vac-VPI-10000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = MEBT-010:Vac-VPI-10000, CHANNEL = 1, CONTROLLERNAME = MEBT-010:Vac-VEPI-10001")

#
# Device: MEBT-010:Vac-VPI-20000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = MEBT-010:Vac-VPI-20000, CHANNEL = 2, CONTROLLERNAME = MEBT-010:Vac-VEPI-10001")

#
# Device: MEBT-010:Vac-VPI-30000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = MEBT-010:Vac-VPI-30000, CHANNEL = 3, CONTROLLERNAME = MEBT-010:Vac-VEPI-10001")

#
# Device: MEBT-010:Vac-VPI-40000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = MEBT-010:Vac-VPI-40000, CHANNEL = 4, CONTROLLERNAME = MEBT-010:Vac-VEPI-10001")

#
# Device: MEBT-010:Vac-VEPI-20001
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_ctrl_digitelqpce_moxa.iocsh", "DEVICENAME = MEBT-010:Vac-VEPI-20001, IPADDR = moxa-vac-mebt.tn.esss.lu.se, PORT = 4006")

#
# Device: MEBT-010:Vac-VPI-50000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = MEBT-010:Vac-VPI-50000, CHANNEL = 1, CONTROLLERNAME = MEBT-010:Vac-VEPI-20001")
