# IOC for MEBT vacuum ion pumps

## Used modules

*   [vac_ctrl_digitelqpce](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_digitelqpce)


## Controlled devices

*   MEBT-010:Vac-VEPI-10001
    *   MEBT-010:Vac-VPI-10000
    *   MEBT-010:Vac-VPI-20000
    *   MEBT-010:Vac-VPI-30000
    *   MEBT-010:Vac-VPI-40000
*   MEBT-010:Vac-VEPI-20001
    *   MEBT-010:Vac-VPI-50000
